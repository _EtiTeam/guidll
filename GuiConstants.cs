﻿namespace GuiDLL
{
    public class GuiConstants
    {
        public const string BASE_SALES_GUI_PATH = @"..\..\..\..\gui-components\Sales\salesGUI\";

        public const string BASE_ACCOUNTANCY_GUI_PATH = @"..\..\..\..\gui-components\Accountancy\accountancyGUI\";

        public const string BASE_SPEDITION_GUI_PATH = @"..\..\..\..\gui-components\Spedition\speditionGUI\";

        public const string BASE_WAREHOUSE_GUI_PATH = @"..\..\..\..\gui-components\Warehouse\warehouseGUI\";

        public const string HTML_TYPE = "text/html";

        public const string CSS_TYPE = "text/css";

        public const string JS_TYPE = "application/javascript";

        public const string IMG_TYPE = "image";
    }
}
